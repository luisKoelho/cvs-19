/*
    Class Drone 

    Construction and Verification of Software, FCT-UNL, © (uso reservado)

<<<<<<< HEAD
    Enunciado:
        This class emulates a drone. The drone has to obey some restrictions. 
        There are restrictions on its speed and position, and on the conditions 
        its operations can execute.
=======
    This class emulates a drone. The drone has to obey some restrictions. 
    There are restrictions on its speed and position, and on the conditions 
    its operations can execute.

    The drone supports 4 operations: take off, move, addWaypoint, and land.
    Take note that for every operation, you'll need to implement a method
    for starting it and another for finishing it. For instance, let us
    consider the operation move which moves the drone from point A to
    point B. This operation is divided into <Move> and <CompleteMove>.
    In <Move>, it is ncessary to set the target destination and the drone's
    speed. In the second part of the operations, <CompleteMove>, it is
    necessary to set the drone's position as being the target position
    (it arrived to the target destination) and set the speed of the drone
    to zero. It is hovering at the target position.

    The drone can freely move inside a semi-sphere with
    radius <r> with center <n> meters above its control station and and in a
    cilinder with a height of <n> meters and whose base is centered in the
    drone's control station. Regarding the speed of the drone, the drone's 
    maximum speed is 10 Km/h.
>>>>>>> 9da63c88710b8bfbdb609b0bba0104ddbe73ab72

        The drone supports 4 operations: take off, move, addWaypoint, and land.
        Take note that for every operation, you'll need to implement a method
        for starting it and another for finishing it. For instance, let us
        consider the operation move which moves the drone from point A to
        point B. This operation is divided into <Move> and <CompleteMove>.
        In <Move>, it is necessary to set the target destination and the drone's
        speed. In the second part of the operations, <CompleteMove>, it is
        necessary to set the drone's position as being the target position
        (it arrived to the target destination) and set the speed of the drone
        to zero. It is hovering at the target position.

        As for restrictions, one of them is that the distance of the drone to its
        control station, which is placed at (x, y, z) = (0, 0, 0) and its
        operational height. The drone can freely move inside a semi-sphere with
        radius <r> with center <n> meters above its control station and and in a
        cilinder with a height of <n> meters and whose base is centered in the
        drone's control station. Regarding the speed of the drone, the drone's 
        maximum speed is 10 Km/h.

    RESTRICTIONS: 
    
    (x, y, z) = (0, 0, 0) -> control station
    >max speed = 10 Km/h
    <r> -> radius
    <n> -> operational height

    OPERATIONS:
    
        Take Off{
            Makes the drone climb to its operational height at maximum speed and hover
            at that position.
            With the exception for its height, its position must not change.
            This operation can only execute successfully if the drone is on the ground
            (landed).

                -takeOff
                > height: 0-> max
                > vel: 0-> max;
                > requires: landed
                > post: move-takingOff
                
                -completeTakeOff
                > vel: max->0;
                > requires: move-takingOff
                > post: idle
        }

        Move{
            Given a valid position, this operation causes the drone to move to the
            target position. When moving, the drone moves at its maximum speed.
            In a similar fashion to the previous operation, this operation only
            executes if the drone is idle.

                -move
                > vel: 0-> max;
                > requires: idle, set the target destination + drone's speed.
                >post: moving
                -completeMove
                > vel: max->0;
                >requires: move-moving, set the drone's position as being the target position(it arrived to the target)
                + set the speed of the drone=0 + It is hovering at the target position (idle)
                > post: idle
        }

        AddWaypoint{
            Given a position IN THE SEMI-SPHERE with center at <n> meters above its
            control station, the drone will add that position to a sequence of
            positions it will follow one by one. When following waypoints, the
            drone is said to be patrolling and it moves at half its maximum
            speed. It is only possible to add new waypoints to the drone if
            it is hovering (idle) or it is already on paatrol.
        }

        -Land{
            Lands the drone directly below its current position. When landing,
            the drone does so at its maximum speed. This operation can only execute
            when the drone is idle.

                -land
                > vel: 0-> max;
                > requires: idle
                > post: move-landing
                
                -completeLand
                > height: -> 0
                > vel: max->0;
                > requires: move-landing
                > post: landed
        }

    EXTRA INFO:
        When instantiating a new drone, it is necessary to supply its
        operational height (<n>) and its operation range (<r>).

        There is a mapping between the representation state and abstract state
        that guarantees the soundness of the ADT. Also, don't forget the
        state invariant.
	    The delivery date is the 3rd May
 */

class Drone {

<<<<<<< HEAD
    var waypoints:seq<(int,int,int)>;

//area limits var
var height:int, range:int;

//drone var
var speed:int,maxSpeed:int;
var status:string;

    //location var
    var droneX:int,droneY:int,droneZ:int;
    var goalX:int,goalY:int,goalZ:int;


constructor(n:int, r: int)

requires n>0;
requires r>0;
ensures RepInv();
{
    waypoints:=[];
    height:=n;
    range:=r;
    maxSpeed:=10;

    droneX:=0;
    droneY:=0;
    droneZ:=0;

    goalX:=0;
    goalY:=0;
    goalZ:=0;

}


function RepInv():bool
reads this;
{ 
    height>0 && range>0 
    &&
    isValid(droneX,droneY,droneZ) &&
    isValid(goalX,goalY,goalZ) &&
    forall pX,pY,pZ:: ((pX,pY,pZ) in waypoints) ==>
   ((pX*pX)+(pY*pY)+((pZ-height)*(pZ-height)) <= (range*range) 
    &&
    pZ>=height && isValid(pX,pY,pZ)
   )
}

function isValid(x:int,y:int,z:int):bool
reads this;
{
 (//dentro da esfera =>( x-cx ) ^2 + (y-cy) ^2 + (z-cz) ^ 2 < r^2 
     (x*x)+(y*y)+((z-height)*(z-height)) <= (range*range) 
  ||
  (// dentro do cilindro
    //dentro do circulo => (x-cx)^2 + (y - cy)^2 < radius^2
    (x*x)+(y*y) <= (range*range) && 0<=z<=height
  ))
  && 
  0<=z<=(height+range)
}

method takeOff()
modifies this;
requires RepInv() && status=="landed"
ensures RepInv() && speed==maxSpeed && status=="moveTO"
{
    speed:=maxSpeed;
    status:="moveTO";
}

method completeTakeOff()
modifies this;
requires RepInv() && status=="moveTO"
ensures RepInv()
ensures speed==0 && status=="idle"
{
    droneZ:=height;
    assert RepInv();
    speed:=0;
    status:="idle";
}

method setupGoal(x:int,y:int,z:int)
    modifies this;
    requires RepInv() && isValid(x,y,z)
    ensures RepInv()
    {
        goalX:=x;
        goalY:=y;
        goalZ:=z;
    }

method move(tX:int,tY:int,tZ:int)
modifies this;
requires RepInv() && isValid(tX,tY,tZ) && status=="idle"
ensures RepInv() && speed==maxSpeed && status=="moving"
{
    setupGoal(tX,tY,tZ);
    speed:=maxSpeed;
    status:="moving";
}

method completeMove()
modifies this;
requires RepInv() && status=="moving"
ensures RepInv()
ensures speed==0 && status=="idle"
{
    droneX:=goalX;
    droneY:=goalY;
    droneZ:=goalZ;

    speed:=0;
    status:="idle";
}

method land()
modifies this;
requires RepInv() && status=="idle"
ensures RepInv() && speed==maxSpeed && status=="moveL"
{
    speed:=maxSpeed;
    status:="moveL";
}

method completeLand()
modifies this;
requires RepInv() && status=="moveL"
ensures RepInv()
ensures droneZ==0 && status=="landed"
{
    droneZ:=0;
    assert RepInv();
    speed:=0;
    status:="landed";
}

method addWaypoint(pX:int,pY:int,pZ:int)
modifies this;
requires RepInv() && (status=="idle" || status=="patrol")
&&
(// wp está dentro da semi-esfera 
 (pX*pX)+(pY*pY)+((pZ-height)*(pZ-height)) <= (range*range)
 &&
 pZ>=height && isValid(pX,pY,pZ)
)
ensures RepInv()
{
waypoints:= waypoints + [(pX,pY,pZ)];
assert waypoints[|waypoints|-1] == (pX,pY,pZ);

if(status=="idle"){
    //caso o drone esteja parado define este novo waypoint
    //como o seu proximo objetivo e entra em modo patrol
    status:="patrol";
    speed:= maxSpeed/2;
    setupGoal(pX,pY,pZ);
}

}

method nextWP()
//a minha implementacao de nextWP tem como objetivo ser usada apenas
//quando estao registados 1 ou mais waypoints e serve tambem como
// "finalizador" do status patrol 
//(tal como os metodos "completeX()" são para os metodos "X")

//só pode ser ativado quando o drone concluiu a viagem ate ao waypoint
//que esta no inicio de waypoints[] (que é automaticamente definido como goal em "addWaypoint")
//o que faz a seguir depende do estado de 
//waypoints[]
modifies this;
requires RepInv() && status=="patrol" &&
//pelo menos 1 waypoint precisa de estar registado
|waypoints|>0 &&
//para poder ativar este metodo é necessário ja 
//ter chegado ao waypoint que esta no inicio da seq
(droneX,droneY,droneZ)==waypoints[0]
ensures RepInv()
{
if (|waypoints|>1){
    //caso 1: ainda ha mais waypoints em espera
    //como já chegou ao waypoint, podemos agora remove-lo da lista 
    waypoints:= waypoints[1..];
    //definimos o waypoint que esta agora em 1º lugar como goal e o status continua em patrol
    var (x,y,z):= waypoints[0];
    setupGoal(x,y,z);
    assert RepInv();
}
else{
    //caso 2: o waypoint que foi finalizado ao entrar neste metodo
    //era o unico na lista de espera.
    
    //resta apenas retirar o waypoint da lista/fazer reset e terminar patrol
    waypoints:=[];
    status:="idle";
    speed:=0;
}

}
=======
    When instantiating a new drone, it is necessary to supply its
    operational height (<n>) and its operation range (<r>).

    There is a mapping between the representation state and abstract state
    that guarantees the soundness of the ADT. Also, don't forget the
    state invariant.
	
	The delivery date is the 29th of April.
 */
>>>>>>> 9da63c88710b8bfbdb609b0bba0104ddbe73ab72


}