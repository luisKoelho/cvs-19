import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class InsertionTests {
	
	private MyIntegerList buildist() {
		// TODO Auto-generated method stub
		int[] MyIntegerList = new int[0]; 
		return null;
	}

    @Test
    public void testPush() {
        final MyIntegerList mil = new MyIntegerList();

        assertEquals(0, mil.size());
        assertEquals("[]", mil.toString());

        mil.push(1);
        assertEquals(1, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[1]", mil.toString());

        mil.push(2);
        assertEquals(2, mil.size());
        assertEquals("[1,2]", mil.toString());

        mil.push(1);
        assertEquals(3, mil.size());
        assertEquals("[1,2,1]", mil.toString());
    } 
    @Test
    public void testInsertAt() {
    	 final MyIntegerList mil=buildist();
    	 
    	 mil.insertAt(0, idx);
    	 assertEquals();
     }
    
    @Test
    void testInsertSorted2() {
    	final MyIntegerList mil=buildList(new int[] {1,2,3});
    	
    	mil.sortedInsertion(0);
    	assertEquals("[0,1,2,3]",mil.toString());
    }
    
    
	@Test
	void testSort() {
		final MyIntegerList mil=buildList(new int[] {1,2,3});
		assertEquals(6,mil.elementsSum());
	}
    
    }

